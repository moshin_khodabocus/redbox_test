<?php

require_once __DIR__ . '/vendor/autoload.php';

use RedboxTest\Classes\User\UserAction;

$userAction = new UserAction();
$adminUserId = $userAction->register('admin@redbox-test.com', 'admin', 'admin');
$guestUserId = $userAction->register('guest@redbox-test.com', 'guest', 'guest');
$userAction->assignRole($adminUserId, 1);
$userAction->assignRole($guestUserId, 8);
