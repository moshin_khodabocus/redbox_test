<?php

require_once __DIR__ . '/vendor/autoload.php';

use RedboxTest\Classes\Todo\TodoAction;
use RedboxTest\Classes\User\UserAction;

$list = [];
$userAction = new UserAction();

if (!$userAction->checkLoginState()) {
    header('Location: /login.php');
} else {
    $todoAction = new TodoAction();
    $list = $todoAction->findAll();
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include '_partials/styles.php'; ?>

    <title>Homepage | To-do List</title>
</head>
<body>
<?php include '_partials/login-region.php'; ?>

<main>
    <div class="forms">
        <form method="get" action="/src/search-action.php" class="form">
            <div class="form-group">
                <label for="title" class="hidden">Search by title...</label>
                <input type="text" class="form-control" id="title" name="q" placeholder="Search by title...">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>

        <?php if ($userAction->hasAdminRights()): ?>
            <form method="post" action="/src/add.php" class="form">
                <div class="form-group">
                    <label for="title" class="hidden">Add a new to-do item...</label>
                    <input type="text" class="form-control" id="title" name="title"
                           placeholder="Add a new to-do item...">
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        <?php endif; ?>
    </div>

    <div class="list">
        <h1>To-do List</h1>

        <?php if (count($list) > 0): ?>
            <?php if ($userAction->hasAdminRights()): ?>
                <p><a href="/src/delete-all.php" class="btn btn-danger">Delete all</a></p>
            <?php endif; ?>
            <ul class="list-group">
                <?php
                foreach ($list as $listItem) {
                    include '_partials/list-item.php';
                }
                ?>
            </ul>
        <?php endif; ?>
    </div>
</main>
</body>
</html>
