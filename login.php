<?php

require_once __DIR__ . '/vendor/autoload.php';

use RedboxTest\Classes\User\UserAction;

$userAction = new UserAction();

if ($userAction->checkLoginState()) {
    header('Location: /index.php');
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include '_partials/styles.php'; ?>

    <title>Login | To-do List</title>
</head>
<body>
<main>
    <h1>Login</h1>
    <form method="post" action="/src/login-action.php">
        <div class="form-group">
            <label for="email" class="hidden">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="password" class="hidden">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</main>
</body>
</html>
