<?php

require_once __DIR__ . '/vendor/autoload.php';

if (!isset($_GET['id']) || empty($_GET['id'])) {
    header('Location: /index.php');
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include '_partials/styles.php'; ?>

    <title>Edit | To-do List</title>
</head>
<body>
<p><a href="/index.php">Go back to homepage</a></p>

<h1>Edit</h1>

<form method="post" action="/src/edit-action.php">
    <div class="form-group">
        <label for="title" class="hidden">Enter new title...</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Enter new title...">
    </div>
    <input type="hidden" name="item_id" value="<?php print $_GET['id']; ?>">
    <button type="submit" class="btn btn-primary">Save</button>
</form>

</body>
</html>
