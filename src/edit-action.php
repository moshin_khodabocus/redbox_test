<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use RedboxTest\Classes\Todo\TodoAction;

if (isset($_POST['item_id']) && !empty($_POST['item_id'])) {
    if (isset($_POST['title']) && !empty($_POST['title'])) {
        $todoAction = new TodoAction();
        $update = $todoAction->update($_POST['item_id'], $_POST['title']);

        if ($update != false && $update > 0) {
            header('Location: /index.php');
        } else {
            header('Location: /error.php');
        }
    }
}
