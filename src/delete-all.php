<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use RedboxTest\Classes\Todo\TodoAction;

$todoAction = new TodoAction();
$todoAction->deleteAll();

header('Location: /index.php');
