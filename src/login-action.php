<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use RedboxTest\Classes\User\UserAction;

if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $userAction = new UserAction();
    $userAction->login($_POST['email'], $_POST['password']);

    header('Location: /index.php');
}