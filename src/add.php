<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use RedboxTest\Classes\Todo\Entity\TodoItem;
use RedboxTest\Classes\Todo\TodoAction;

if (isset($_POST['title']) && !empty($_POST['title'])) {
    $todoItem = new TodoItem($_POST['title']);
    $todoAction = new TodoAction();
    $add = $todoAction->add($todoItem);

    if ($add != false && $add > 0) {
        header('Location: /index.php');
    } else {
        header('Location: /error.php');
    }
} else {
    header('Location: /index.php');
}

