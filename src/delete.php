<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use RedboxTest\Classes\Todo\TodoAction;

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $todoAction = new TodoAction();
    $delete = $todoAction->delete($_GET['id']);
    $referrer = $_SERVER['HTTP_REFERER'];

    if ($delete != false && $delete > 0) {
        header('Location: ' . $referrer);
    } else {
        header('Location: /error.php');
    }

} else {
    header('Location: /index.php');
}
