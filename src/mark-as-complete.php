<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use RedboxTest\Classes\Todo\TodoAction;

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $todoAction = new TodoAction();
    $markASComplete = $todoAction->markAsComplete($_GET['id']);
    $referrer = $_SERVER['HTTP_REFERER'];

    if ($markASComplete != false && $markASComplete > 0) {
        header('Location: ' . $referrer);
    } else {
        header('Location: /error.php');
    }
} else {
    header('Location: /index.php');
}