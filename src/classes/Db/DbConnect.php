<?php

namespace RedboxTest\Classes\Db;

use PDO;

class DbConnect
{
    private static $instance = null;
    private $connection;

    private $host = 'localhost';
    private $username = 'root';
    private $password = '';
    private $dbName = 'redbox_test';

    private function __construct()
    {
        $this->connection = new PDO(
            "mysql:host={$this->host};dbname={$this->dbName}",
            $this->username,
            $this->password,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
        );
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new DbConnect();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }
}