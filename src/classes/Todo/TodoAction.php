<?php

namespace RedboxTest\Classes\Todo;

use PDO;
use RedboxTest\Classes\Db\DbConnect;
use RedboxTest\Classes\Todo\Entity\TodoItem;

require_once dirname(dirname(dirname(__DIR__))) . '/vendor/autoload.php';

class TodoAction implements TodoActionInterface
{
    private $dbConnect;

    public function __construct()
    {
        $this->dbConnect = DbConnect::getInstance()->getConnection();
    }

    public function add(TodoItem $todoItem)
    {
        try {
            $query = $this->dbConnect->prepare('INSERT INTO todo_item(title) VALUES(:title)');
            $query->execute(['title' => $todoItem->getTitle()]);

            return $query->rowCount();
        } catch (\Exception $e) {
            echo 'Cannot add item: ' . $e->getMessage();
        }

        return false;
    }

    public function delete($id)
    {
        try {
            $query = $this->dbConnect->prepare('DELETE FROM todo_item WHERE id = :id');
            $query->execute(['id' => $id]);

            return $query->rowCount();
        } catch (\Exception $e) {
            echo 'Failed to delete item with id: ' . $id . '. Error message: ' . $e->getMessage();
        }

        return false;
    }

    public function deleteAll()
    {
        $query = $this->dbConnect->prepare('DELETE FROM todo_item');
        $query->execute();

        return $query->rowCount();
    }

    public function findAll(): array
    {
        $query = $this->dbConnect->query('SELECT * FROM todo_item');
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function find($title): array
    {
        $likeTitle = '%' . $title . '%';
        $query = $this->dbConnect->prepare('SELECT * FROM todo_item WHERE title LIKE :title');
        $query->execute(['title' => $likeTitle]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function markAsComplete($id)
    {
        try {
            $query = $this->dbConnect->prepare('UPDATE todo_item SET completion_state = :completion_state WHERE id = :id');
            $query->execute(['completion_state' => 1, 'id' => $id]);

            return $query->rowCount();
        } catch (\Exception $e) {
            echo 'Failed to mark the item with id ' . $id . ' as complete. Error message: ' . $e->getMessage();
        }

        return false;
    }

    public function update($id, $title)
    {
        try {
            $query = $this->dbConnect->prepare('UPDATE todo_item SET title = :title WHERE id = :id');
            $query->execute(['title' => $title, 'id' => $id]);

            return $query->rowCount();
        } catch (\Exception $e) {
            echo 'Failed to update the item with id: ' . $id . '. Error message: ' . $e->getMessage();
        }

        return false;
    }

}