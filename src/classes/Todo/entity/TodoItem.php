<?php

namespace RedboxTest\Classes\Todo\Entity;

class TodoItem
{
    private $title;
    private $completionState;

    public function __construct($title, $completionState = 0)
    {
        $this->title = $title;
        $this->completionState = $completionState;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getCompletionState()
    {
        return $this->completionState;
    }

    public function setCompletionState($completionState)
    {
        $this->completionState = $completionState;
    }
}