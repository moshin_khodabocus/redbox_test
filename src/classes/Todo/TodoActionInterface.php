<?php

namespace RedboxTest\Classes\Todo;

use RedboxTest\Classes\Todo\Entity\TodoItem;

interface TodoActionInterface
{
    public function add(TodoItem $todoItem);

    public function delete($id);

    public function deleteAll();

    public function findAll(): array;

    public function find($title): array;

    public function markAsComplete($id);

    public function update($id, $title);
}
