<?php

namespace RedboxTest\Classes\User;

use Delight\Auth\Auth;
use Delight\Auth\InvalidEmailException;
use Delight\Auth\InvalidPasswordException;
use Delight\Auth\TooManyRequestsException;
use Delight\Auth\UnknownIdException;
use Delight\Auth\UserAlreadyExistsException;
use RedboxTest\Classes\Db\DbConnect;

require_once dirname(dirname(dirname(__DIR__))) . '/vendor/autoload.php';

class UserAction implements UserActionInterface
{
    private $auth;

    public function __construct()
    {
        $this->auth = new Auth(DbConnect::getInstance()->getConnection());
    }

    public function login($username, $password)
    {
        $this->auth->login($username, $password);
    }

    public function logout()
    {
        $this->auth->logOut();
    }

    public function register($email, $password, $username)
    {
        try {
            return $this->auth->register($email, $password, $username);
        } catch (InvalidEmailException $e) {
            die('Invalid email address');
        } catch (InvalidPasswordException $e) {
            die('Invalid password');
        } catch (UserAlreadyExistsException $e) {
            die('User already exists');
        } catch (TooManyRequestsException $e) {
            die('Too many requests');
        }
    }

    public function checkLoginState(): bool
    {
        return $this->auth->isLoggedIn();
    }

    public function assignRole($userId, $role)
    {
        try {
            $this->auth->admin()->addRoleForUserById($userId, $role);
        } catch (UnknownIdException $e) {
            die('Unknown user ID');
        }
    }

    public function hasAdminRights(): bool
    {
        return $this->auth->hasRole(1);
    }
}