<?php

namespace RedboxTest\Classes\User;

interface UserActionInterface
{
    public function login($username, $password);

    public function logout();

    public function register($email, $password, $username);

    public function checkLoginState(): bool;

    public function assignRole($userId, $role);

    public function hasAdminRights(): bool;
}
