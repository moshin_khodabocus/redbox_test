<?php

require_once __DIR__ . '/vendor/autoload.php';

use RedboxTest\Classes\Todo\TodoAction;
use RedboxTest\Classes\User\UserAction;

$list = [];

$userAction = new UserAction();

if (!$userAction->checkLoginState()) {
    header('Location: /login.php');
} else {
    if (isset($_GET['q']) && !empty($_GET['q'])) {
        $todoAction = new TodoAction();
        $list = $todoAction->find($_GET['q']);
    } else {
        $todoAction = new TodoAction();
        $list = $todoAction->findAll();
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include '_partials/styles.php'; ?>

    <title>Search | To-do List</title>
</head>
<body>
<?php include '_partials/login-region.php'; ?>
<p><a href="/index.php">Go back to homepage</a></p>

<main>
    <div class="forms">
        <form method="get" action="/src/search-action.php" class="form">
            <div class="form-group">
                <label for="title" class="hidden">Search by title...</label>
                <input type="text" class="form-control" id="title" name="q" placeholder="Search by title...">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <div class="list">
        <h1>Search result</h1>

        <?php if (count($list) > 0): ?>
            <ul class="list-group">
                <?php
                foreach ($list as $listItem) {
                    include '_partials/list-item.php';
                }
                ?>
            </ul>
        <?php endif; ?>
    </div>
</main>
</body>
</html>
