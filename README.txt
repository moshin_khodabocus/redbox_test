Introduction:
This is a to-do list web application designed and developed in plain php. A third-party library,
namely "delight-im/PHP-Auth", was used for the login part, and was installed using Composer.
For the HTML/CSS part, Twitter Bootstrap was used, using their CDN as source for the files.
Finally, no javascript was used as it was not necessary for the test.

Technical Requirements:
- Apache Version: 2.4.37
- PHP Version: 7.1.24
- MySQL Version: 10.1.37-MariaDB
- Composer Version: 1.8.5

Installation:
1. Clone the project locally using the url "https://bitbucket.org/moshin_khodabocus/redbox_test.git"
2. Run the script "/db-tables.sql" to import all necessary tables and data.
3. Run "composer install" to install the third-party login library.
4. Visit the app at your preferred url (using a vhost or simply http://localhost/redbox_test).
5. To test out the two user modes, use the credentials below:
    - Admin:
        - Username: admin@redbox-test.com
        - Password: admin
    Guest:
        - Username: guest@redbox-test.com
        - Password: guest