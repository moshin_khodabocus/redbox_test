<?php

require_once 'vendor/autoload.php';

use RedboxTest\Classes\User\UserAction;

$userAction = new UserAction();
if ($userAction->checkLoginState()) {
    $userAction->logout();
    header('Location: /login.php');
}
