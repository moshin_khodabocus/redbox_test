<li class="list-group-item<?php if ($listItem['completion_state'] == 1) {
    print ' list-group-item-success';
} ?>">
    <div class="title">
        <span><?php print $listItem['title']; ?></span>
    </div>

    <?php if ($userAction->hasAdminRights()): ?>
        <div class="actions">
            <?php if ($listItem['completion_state'] == 0): ?>
                <a href="/src/mark-as-complete.php?id=<?php print $listItem['id']; ?>" class="btn btn-success">Mark as
                    complete</a>
            <?php endif; ?>
            <a href="/edit.php?id=<?php print $listItem['id']; ?>" class="btn btn-primary">Edit</a>
            <a href="/src/delete.php?id=<?php print $listItem['id']; ?>" class="btn btn-danger">Delete</a>
        </div>
    <?php endif; ?>
</li>